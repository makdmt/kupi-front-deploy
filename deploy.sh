#!/bin/bash
DEPLOY_PATH='/home/dm/praktikum/kupipodari/kupipodari-deploy'
FRONTEND_CONTAINER_NAME=frontend

cd $DEPLOY_PATH
docker compose down $FRONTEND_CONTAINER_NAME
docker rmi $FRONTEND_CONTAINER_NAME
docker load --input $FRONTEND_CONTAINER_NAME.tar
docker compose up $FRONTEND_CONTAINER_NAME -d
docker ps
echo $(pwd)